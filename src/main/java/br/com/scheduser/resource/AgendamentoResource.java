package br.com.scheduser.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.entity.Cliente;
import br.com.scheduser.service.AgendamentoService;

@Path( "/api/agendamento" )
public class AgendamentoResource {

	@Inject
	AgendamentoService agservice;
	
	@GET
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllAgendamentos() {
		List< Agendamento > agendamentos = this.agservice.getAllAgendamentos();
		return Response.ok( agendamentos ).build();
	}
	
	@GET
	@Path( "/{id}")
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllAgendamentos( @PathParam( value = "codigo") Integer codAgendamento) {
		Agendamento agendamentos = this.agservice.getByIdAgendamento(codAgendamento);
		return Response.ok( agendamentos ).build();
	}
	
	@POST
	@Produces( MediaType.APPLICATION_JSON )
	@Consumes( MediaType.APPLICATION_JSON )
	public Response saveAgendamento( Agendamento agendamentos ) {
		this.agservice.saveAgendamento(agendamentos);
		return Response.accepted().build();
	}
}
