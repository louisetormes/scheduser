package br.com.scheduser.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.scheduser.entity.Cliente;
import br.com.scheduser.service.ClienteService;

@Path( "/api/cliente" )
public class ClienteResource {

	@Inject
	ClienteService clservice;
	
	@GET
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllClientes() {
		List< Cliente > clientes = this.clservice.getAllClientes();
		return Response.ok( clientes ).build();
	}
	
	@GET
	@Path( "/{id}")
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllClientes( @PathParam( value = "id") String cpf) {
		Cliente clientes = this.clservice.getByIdCliente(cpf);
		return Response.ok( clientes ).build();
	}
	
	@POST
	@Produces( MediaType.APPLICATION_JSON )
	@Consumes( MediaType.APPLICATION_JSON )
	public Response saveCliente( Cliente clientes ) {
		this.clservice.saveCliente(clientes);
		return Response.accepted().build();
	}
}
