package br.com.scheduser.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.scheduser.entity.Servicos;
import br.com.scheduser.service.ServicosService;

@Path( "/api/servicos" )
public class ServicosResource {

	@Inject
	ServicosService svservice;
	
	@GET
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllServicos() {
		List< Servicos > servicos = this.svservice.getAllServicos();
		return Response.ok( servicos ).build();
	}
	
	@GET
	@Path( "/{id}")
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllServicos( @PathParam( value = "id") Integer codServico) {
		Servicos servicos = this.svservice.getByIdServicos(codServico);
		return Response.ok( servicos ).build();
	}
	
	@POST
	@Produces( MediaType.APPLICATION_JSON )
	@Consumes( MediaType.APPLICATION_JSON )
	public Response saveServicos( Servicos servicos ) {
		this.svservice.saveServicos(servicos);
		return Response.accepted().build();
	}
}
