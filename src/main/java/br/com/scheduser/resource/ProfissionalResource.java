package br.com.scheduser.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.com.scheduser.entity.Profissional;
import br.com.scheduser.service.ProfissionalService;

@Path( "/api/profissional")
public class ProfissionalResource {

	@Inject
	ProfissionalService prservice;
	
	@GET
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllProfissionais() {
		List< Profissional > profissionais = this.prservice.getAllProfissionais();
		return Response.ok( profissionais ).build();
	}
	
	@GET
	@Path( "/{id}")
	@Produces( MediaType.APPLICATION_JSON )
		public Response getAllProfissionais( @PathParam( value = "id") String cpfProfissional) {
		Profissional profissionais = this.prservice.getByIdProfissional(cpfProfissional);
		return Response.ok( profissionais ).build();
	}
	
	@POST
	@Produces( MediaType.APPLICATION_JSON )
	@Consumes( MediaType.APPLICATION_JSON )
	public Response saveCliente( Profissional profissionais ) {
		this.prservice.saveProfissional(profissionais);
		return Response.accepted().build();
	}
}
