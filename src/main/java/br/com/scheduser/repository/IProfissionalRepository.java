package br.com.scheduser.repository;

import java.util.List;

import br.com.scheduser.entity.Profissional;

public interface IProfissionalRepository {

void saveProfissional( Profissional profissional );
	
	List< Profissional > getAllProfissional();
	
	Profissional getByIdProfissionais( String cpfProfissional);
}
