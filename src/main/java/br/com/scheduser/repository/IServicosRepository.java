package br.com.scheduser.repository;

import java.util.List;

import br.com.scheduser.entity.Servicos;

public interface IServicosRepository {

void saveServicos( Servicos servicos );
	
	List< Servicos > getAllServicos();
	
	Servicos getByIdServicos( Integer codServico);
}
