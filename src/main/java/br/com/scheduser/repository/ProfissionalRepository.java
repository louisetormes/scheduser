package br.com.scheduser.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.scheduser.entity.Profissional;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

	@ApplicationScoped
	public class ProfissionalRepository implements IProfissionalRepository, PanacheRepositoryBase<Profissional, String>{

		@Override
		@Transactional
		public void saveProfissional( Profissional profissional ) {
			this.persist(profissional);
	    	}
		
		@Override
		public List< Profissional > getAllProfissional() {
			return this.findAll().list();
		}
		
		@Override
		public Profissional getByIdProfissionais ( String cpfProfissional ) {
			return this.findById(cpfProfissional);
		}
}
