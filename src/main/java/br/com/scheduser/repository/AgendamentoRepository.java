package br.com.scheduser.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.scheduser.entity.Agendamento;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class AgendamentoRepository implements IAgendamentoRepository, PanacheRepositoryBase<Agendamento, Integer> {

	@Override
	@Transactional
	public void saveAgendamento(Agendamento agendamento) {
		this.persist(agendamento);
	}

	@Override
	public List<Agendamento> getAllAgendamentos() {
		return this.findAll().list();
	}

	@Override
	public Agendamento getByIdAgendamento(Integer codAgendamento) {
		return this.findById(codAgendamento);
	}

}
