package br.com.scheduser.repository;

import java.util.List;

import br.com.scheduser.entity.Agendamento;

public interface IAgendamentoRepository {

	void saveAgendamento(Agendamento agendamento);

	List<Agendamento> getAllAgendamentos();

	Agendamento getByIdAgendamento(Integer codAgendamento);
}
