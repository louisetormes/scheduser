package br.com.scheduser.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.scheduser.entity.Servicos;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

	@ApplicationScoped
	public class ServicosRepository implements IServicosRepository, PanacheRepositoryBase<Servicos, Integer>{

		@Override
		@Transactional
		public void saveServicos( Servicos servicos ) {
			this.persist(servicos);
	    	}
		
		@Override
		public List< Servicos > getAllServicos() {
			return this.findAll().list();
		}
		
		@Override
		public Servicos getByIdServicos ( Integer codServico ) {
			return this.findById(codServico);
		}
}

