package br.com.scheduser.repository;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;

import br.com.scheduser.entity.Cliente;
import io.quarkus.hibernate.orm.panache.PanacheRepositoryBase;

@ApplicationScoped
public class ClienteRepository implements IClienteRepository, PanacheRepositoryBase<Cliente, String> {

	@Override
	@Transactional
	public void saveCliente(Cliente cliente) {
		this.persist(cliente);
	}

	@Override
	public List<Cliente> getAllClientes() {
		return this.findAll().list();
	}

	@Override
	public Cliente getByIdCliente(String cpf) {
		return this.findById(cpf);
	}

}
