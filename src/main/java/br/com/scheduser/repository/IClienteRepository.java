package br.com.scheduser.repository;

import java.util.List;

import br.com.scheduser.entity.Cliente;

public interface IClienteRepository {

void saveCliente( Cliente cliente);
	
	List< Cliente > getAllClientes();
	
	Cliente getByIdCliente ( String cpf );
}
