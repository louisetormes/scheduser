package br.com.scheduser.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.scheduser.entity.Servicos;
import br.com.scheduser.repository.IServicosRepository;

@ApplicationScoped
public class ServicosService {

	@Inject
	IServicosRepository svrepository;
	
	public void saveServicos( Servicos servico ) {
		this.svrepository.saveServicos(servico);
	}
	
	public List< Servicos > getAllServicos() {
		return this.svrepository.getAllServicos();
	}
	
	public Servicos getByIdServicos( Integer codServico ) {
		return this.svrepository.getByIdServicos(codServico);
	}
}
