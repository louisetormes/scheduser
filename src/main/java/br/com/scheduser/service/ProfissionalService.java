package br.com.scheduser.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.scheduser.entity.Profissional;
import br.com.scheduser.repository.IProfissionalRepository;

@ApplicationScoped
public class ProfissionalService {

	@Inject
	IProfissionalRepository prrepository;
	
	public void saveProfissional( Profissional profissional ) {
		this.prrepository.saveProfissional(profissional);
	}
	
	public List< Profissional > getAllProfissionais() {
		return this.prrepository.getAllProfissional();
	}
	
	public Profissional getByIdProfissional( String cpfProfissional ) {
		return this.prrepository.getByIdProfissionais(cpfProfissional);
	}
}
