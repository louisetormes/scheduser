package br.com.scheduser.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.scheduser.entity.Cliente;
import br.com.scheduser.repository.IClienteRepository;

@ApplicationScoped
public class ClienteService {

	@Inject
	IClienteRepository clrepository;
	
	public void saveCliente( Cliente cliente ) {
		this.clrepository.saveCliente( cliente );
	}
	
	public List< Cliente > getAllClientes() {
		return this.clrepository.getAllClientes();
	}
	
	public Cliente getByIdCliente( String cpf ) {
		return this.clrepository.getByIdCliente(cpf);
	}
}
