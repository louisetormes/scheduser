package br.com.scheduser.service;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import br.com.scheduser.entity.Agendamento;
import br.com.scheduser.repository.IAgendamentoRepository;

@ApplicationScoped
public class AgendamentoService {
	
	@Inject
	IAgendamentoRepository agrepository;
	
	public void saveAgendamento( Agendamento agendamento ) {
		this.agrepository.saveAgendamento(agendamento);
	}
	
	public List< Agendamento > getAllAgendamentos() {
		return this.agrepository.getAllAgendamentos();
	}

	public Agendamento getByIdAgendamento( Integer codAgendamento ) {
		return this.agrepository.getByIdAgendamento(codAgendamento);
	}
}
